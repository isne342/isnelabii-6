//
//  main.cpp
//  challenge#6ISNELabII
//
//  Created by Panpech Pothong on 10/12/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//
#include <iostream>
#include <list>
#include <queue>
#include <string>
#include "Header.h"
using namespace std;

int main()
{
    Graph<int> graph;
    bool check_weight = false,check_complete = true;
    int row,collum;
    cout << "How many row and collum"<<endl;
    cin>>row;
    cin>>collum;
    int  arr[row][collum];
    for (int i=0; i<row; i++) {
        for (int j=0; j<collum; j++) {
            cin >> arr[i][j];
        }
    }
    queue<int> arr_hold;
    graph.start(4);
    for (int y = 0; y < row; y++)
    {
        for (int x = 0; x < collum; x++)
        {
            if (arr[y][x] != 0)
            {
                graph.insert(arr[y][x] + ((x+1)*1000), y + 1);
                check_weight = true;
            }
            else { check_complete = false; }
        }
    }
    
    if (graph.check_multi())
    {
        cout << "This is MutiGraph" << endl;
    }
   
    if (graph.check_pseudo())
    {
        cout << "This is PsudoGraph" << endl;
    }
    if(check_weight)
    {
        cout << "This is Weighted Graph" << endl;
    }
  
    if (graph.check_di())
    {
        cout << "This is Directed Graph" << endl;
    }
 
    if (check_complete)
    {
        cout << "This isComplete Graph" << endl;
    }
}



