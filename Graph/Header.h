//
//  header.h
//  challenge#6ISNELabII
//
//  Created by Panpech Pothong on 10/19/2559 BE.
//  Copyright © 2559 Panpech Pothong. All rights reserved.
//

#ifndef Graph
#include <queue>
#include <stack>
using namespace std;

template<class T> class Graph;

template<class T>
class Node {
public:
    Node() { left = right = NULL; }
    Node(const T& el, Node *l = 0, Node *r = 0) {
        key = el; left = l; right = r;
    }
    T key;
    Node *left, *right;
};

template<class T>
class Graph {
public:
    Graph() { root = 0; }
    ~Graph() { clear(); }
    void clear() { clear(root); root = 0; }
    bool isEmpty() { return root == 0; }
    void inorder() { inorder(root); }
    void insert(const T& el,int row);
    void deleteNode(Node<T> *& node);
    void visit(Node<T> *p);
    void start(int num);
    bool check_multi();
    bool check_pseudo();
    bool check_di();
    
protected:
    Node<T> *root;
    
    void clear(Node<T> *p);
    void inorder(Node<T> *p);
    
};

template<class T>
void Graph<T>::clear(Node<T> *p)
{
    if (p != 0) {
        clear(p->left);
        clear(p->right);
        delete p;
    }
}

template<class T>
void Graph<T>::inorder(Node<T> *p) {
    
    if (root != NULL) {
        inorder(p->left);
        visit(p);
        inorder(p->right);
        
    }
    else {
        cout << " The tree is empty" << endl;
    }
    
}
template<class T>
void Graph<T>::insert(const T &el,int row) {
    Node<T> *p = root, *prev = 0,*temp=root;
    for (int i = 1; i < row; i++)
    {
        temp = temp->left;
    }
    p = temp;
    while (p != 0) {
        prev = p;
        p = p->right;
    }
    if (root == 0)
        root = new Node<T>(el);
    else
        prev->right = new Node<T>(el);
}

template<class T>
void Graph<T>::deleteNode(Node<T> *&node) {
    Node<T> *prev, *tmp = node;
    if (node->right == 0)
        node = node->left;
    else if (node->left == 0)
        node = node->right;
    else {
        tmp = node->left;
        prev = node;
        while (tmp->right != 0) {
            prev = tmp;
            tmp = tmp->right;
        }
        node->key = tmp->key;
        if (prev == node)
            prev->left = tmp->left;
        else prev->right = tmp->left;
    }
    delete tmp;
}
template<class T>
void Graph<T>::visit(Node<T> *p)
{
    cout << p->key << endl;
}
template<class T>
inline void Graph<T>::start(int num)
{
    for (int i = 0; i < num; i++)
    {
        Node<T> *p = root, *prev = 0;
        while (p != 0)
        {
            prev = p;
            p = p->left;
        }
        if (root == 0)
        {
            root = new Node<T>(i+1);
        }
        else
            prev->left = new Node<T>(i+1);
    }
    
    
}
template<class T>
inline bool Graph<T>::check_multi()
{
    int  hold1 = 0, hold2 = 0;
    Node<T> *p = root,*temp = root;
    while (p != 0)
    {
        temp = p->right;
        
        while (temp->right != 0)
        {
            
            hold1 = temp->key;
            hold2 = temp->right->key;
            if (hold1/1000 == hold2/1000)
            {
                return true;
            }
            else
            {
                temp = temp->right;
            }
        }
        p = p->left;
    }
    
    return false;
}
template<class T>
inline bool Graph<T>::check_pseudo()
{
    T  hold1 = 0, hold2 = 0;
    Node<T> *p = root, *temp = root;
    while (p != 0)
    {
        hold1 = p->key;
        hold1 = hold1;
        temp = p->right;
        while (temp != 0)
        {
            hold2 = temp->key;
            
            if (hold1 == hold2/1000)
            {
                return true;
            }
            else
            {
                temp = temp->right;
            }
        }
        p = p->left;
    }
    
    return false;
}
template<class T>
inline bool Graph<T>::check_di()
{
    int hold1, hold2;
    Node<T> *p = root;
    hold1 = p->right->key;
    p = p->left;
    hold2 = p->right->key;
    if (hold1 % 1000 == hold2 % 1000)
    {
        return true;
    }
    else
        return false;
}
#endif
